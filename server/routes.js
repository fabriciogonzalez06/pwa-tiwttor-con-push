// Routes.js - Módulo de rutas
var express = require('express');
var router = express.Router();

const push = require('./push');


const mensajes = [

    {
        _id: 'XXX',
        user: 'spiderman',
        mensaje: 'Hola Mundo'
    }

];


// Get mensajes
router.get('/', function(req, res) {
    // res.json('Obteniendo mensajes');
    res.json(mensajes);
});


// Post mensaje
router.post('/', function(req, res) {

    const mensaje = {
        mensaje: req.body.mensaje,
        user: req.body.user
    };

    mensajes.push(mensaje);

    console.log(mensajes);


    res.json({
        ok: true,
        mensaje
    });
});

//almacenar la susbcripcion
router.post('/subscribe', (req, res) => {

    const subscripcion = req.body;

    console.log(subscripcion);

    //guardar subscripcion
    push.addSubscription(subscripcion);

    res.json('subscribe');
});

//obtener el key publico
router.get('/key', (req, res) => {

    let key = push.getKey();

    res.send(key);
});

//enviar notificacion push a las personas que nosotros queramos
//ESTO ES ALGO QUE SE OCNTROLA DEL LADO DEL SERVIDOR
router.post('/push', (req, res) => {

    const post = {
        titulo: req.body.titulo,
        cuerpo: req.body.cuerpo,
        usuario: req.body.usuario
    }

    push.sendPush(post);
    res.json(post);
});


module.exports = router;