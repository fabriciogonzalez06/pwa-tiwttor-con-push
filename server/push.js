const vapid = require('./vapid.json');
const urlsafeBase64 = require('urlsafe-base64');
const fs = require('fs');
const webpush = require('web-push');

webpush.setVapidDetails(
    'mailto:angelfabriciogonzalez@gmail.com',
    vapid.publicKey,
    vapid.privateKey
);

let suscripciones = [];

let getSuscripciones = () => {

    if (require('./subs-db.json')) {
        suscripciones = require('./subs-db.json');
    }

}


module.exports.getKey = () => {
    return urlsafeBase64.decode(vapid.publicKey);
}



module.exports.addSubscription = (suscripcion) => {

    getSuscripciones();
    suscripciones.push(suscripcion);

    fs.writeFileSync(`${__dirname}/subs-db.json`, JSON.stringify(suscripciones));

}


module.exports.sendPush = (post) => {
    //manda un post atraves del push
    console.log("entro sendPush");
    //enviar mensaje a todas las subscripciones
    getSuscripciones();

    const notificaionesEnviadas = [];

    suscripciones.forEach((suscripcion, i) => {

        const pushProm = webpush.sendNotification(suscripcion, JSON.stringify(post))
            .then(console.log("Notificacion enviada"))
            .catch(err => {

                console.log("Notificacion fallo");

                if (err.statusCode === 410) { //GONE, ya no existe
                    suscripciones[i].borrar = true;
                }
            });

        notificaionesEnviadas.push(pushProm);
    });

    Promise.all(notificaionesEnviadas).then(() => {

        //retornar solo las que no tengan borrar true
        suscripciones = suscripciones.filter(subs => !subs.borrar);

        fs.writeFileSync(`${__dirname}/subs-db.json`, JSON.stringify(suscripciones));
    });
}